class SearchesController < ApplicationController

  def index

    @users = User.all

    @users = User.where("name LIKE ? ", "%#{params[:username]}%") if @username.present?
    @users = User.where("gender LIKE ? ", "%#{params[:gender]}%") if @gender.present?
    return @users
  end

  def new
    @username = Search.new(params[:username])
    @gender = Search.new(params[:gender])
  end

  def create
    @username.save
    @gender.save
  end

  private

  def search_params
    params.require(:search).permit(:users, :gender)
  end

end
