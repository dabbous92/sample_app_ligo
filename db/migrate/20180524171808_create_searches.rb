class CreateSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :searches do |t|
      t.string :users
      t.string :gender

      t.timestamps
    end
  end
end
