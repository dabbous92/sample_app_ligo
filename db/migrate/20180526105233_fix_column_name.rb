class FixColumnName < ActiveRecord::Migration[5.1]
  def change
   change_column :searches, :users, :username
  end
end
